package com;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.iit.demo.util.CalculatorUtil;

public class somthingTest {

	@Test
	public void sumShouldReturnCorrectSum() {
		assertThat(CalculatorUtil.sum(1, 3)).isEqualTo(4);
	}
}
